package casper

import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.random.Random


fun argToMap(items: Iterable<String>): Map<String, String> {
	val argMap = mutableMapOf<String, String>()
	items.forEach {
		val pair = it.split("=")
		if (pair.size == 2) {
			argMap[pair[0]] = pair[1]
		}
	}
	return argMap
}

fun <T, Result> Iterable<T>.collapse(initial: Result, operation: (T, Result) -> Result): Result {
	var next = initial
	forEach {
		next = operation(it, next)
	}
	return next
}

fun <T> randomItem(random: Random, items: Collection<Pair<T, Double>>): T? {
	if (items.isEmpty()) return null
	if (items.size == 1) return items.first().first

	val summaryChance = items.collapse(0.0) { pair, nextChance ->
		if (nextChance < 0.0) throw Error("Chance must be non-negative")
		nextChance + pair.second
	}

	if (summaryChance <= 0.0) return null

	val chance = random.nextDouble(summaryChance)
	var accumulated = 0.0
	items.forEach {
		accumulated += it.second
		if (accumulated >= chance) {
			return it.first
		}
	}
	return items.last().first
}

fun main(args: Array<String>) {
	val map = argToMap(args.asIterable())

	val commonPath = map["path"] ?: ""

	val image1 = map["image1"]
	val image2 = map["image2"]
	val image3 = map["image3"]
	val image4 = map["image4"]
	val image5 = map["image5"]

	val weight1 = map["weight1"]?.toDoubleOrNull()
	val weight2 = map["weight2"]?.toDoubleOrNull()
	val weight3 = map["weight3"]?.toDoubleOrNull()
	val weight4 = map["weight4"]?.toDoubleOrNull()
	val weight5 = map["weight5"]?.toDoubleOrNull()

	val outputArg = map["output"] ?: throw Error("output not set")

	val seed = map["seed"]?.toIntOrNull() ?: 0

	val outputPath = commonPath + outputArg

	val paths = listOf(image1, image2, image3, image4, image5)
	val weights = listOf(weight1, weight2, weight3, weight4, weight5)

	val pathAndWeights = mutableListOf<Pair<String, Double>>()

	for (i in 1..5) {
		val index = i - 1
		val path = paths[index]
		val weight = weights[index]
		if (path == null && weight == null) continue
		if (path == null) {
			throw Error("You must set image$i for weight$i")
		}
		if (weight == null) {
			throw Error("You must set weight$i for image$i")
		}

		pathAndWeights.add(Pair(path, weight))
	}

	if (pathAndWeights.size < 2) {
		throw Error("You must set two pair {weightX & imageX} as min")
	}

	val fileAndWeights = pathAndWeights.map { Pair(File(it.first), it.second) }
	val imageAndWeights = fileAndWeights.map {
		if (!it.first.exists()) {
			throw Error("File ${it.first.absoluteFile} not found")
		}
		Pair(ImageIO.read(it.first), it.second)
	}

	val firstImage = imageAndWeights.first().first

	imageAndWeights.forEach {
		val nextImage = it.first
		if (nextImage.width != firstImage.width || nextImage.height != firstImage.height) {
			throw Error("First image size (${firstImage.width}x${firstImage.height}) <> some image size (${nextImage.width}x${nextImage.height})")
		}
	}

	val outputImage = BufferedImage(firstImage.width, firstImage.height, firstImage.type)

	val random = Random(seed)

	for (x in 0 until firstImage.width) {
		for (y in 0 until firstImage.height) {
			val image = randomItem(random, imageAndWeights)
					?: throw Error("Invalid random image")

			val color = image.getRGB(x, y)

			outputImage.setRGB(x, y, color)
		}
	}

	val pathList = fileAndWeights.map { it.first.path }.joinToString(" + ")

	ImageIO.write(outputImage, "png", File(outputPath))
	println("completed ($pathList = $outputPath)")
}