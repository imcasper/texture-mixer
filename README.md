# texture-mixer

sample arguments:
`image1=data/rock.png image2=data/soil.png output=data/output.png weight1=0.25 weight2=0.75`

path - (optional) common path prefix

image1, image2, image3, image3, image4, image5 - path to next image
weight1, weight2, weight3, weight3, weight4, weight5 - weight for next image

output - path for output image

You must set image & weight by pair.
You must set min two pairs